package me.duzhi.ilog.cms;

import com.jfinal.log.Log;
import io.jpress.utils.StringUtils;
import me.duzhi.ilog.cms.function.Functions;
import org.eclipse.jetty.server.AsyncNCSARequestLog;
import org.eclipse.jetty.server.NCSARequestLog;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.webapp.WebAppContext;

/**
 * Copyright (c) 二月,28,2017 (ashang.peng@aliyun.com) <p> Licensed under the Apache License, Version
 * 2.0 (the "License"); you may not use this file except in compliance with the License. You may
 * obtain a copy of the License at <p> http://www.apache.org/licenses/LICENSE-2.0 <p> Unless
 * required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

public class Startup {

    public static final Log log = Log.getLog(Startup.class);
    static Integer port = Integer.valueOf(System.getProperty("port", "80"));
    static String group = System.getProperty("group", "ilogcms");
    static String webDir = System.getProperty("webDir", "dir");

    public static void main(String[] args) throws Exception {
        // 设置线程名称
        Thread.currentThread().setName(group + "_" + port);
        log.info("------- ILogCMS Start Up   -----------");
        log.info("-- Version : V0.1          -----------");
        log.info("-- Base on :Jress 0.8      -----------");
        log.info("-- Use Jinal .etc          -----------");
        log.info("-- Port :" + port + "     -----------");
        final Server server = new Server(Integer.valueOf(port));
        try {
            WebAppContext context = new WebAppContext(webDir, "/");
            context.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false");
            context.setInitParameter("org.eclipse.jetty.servlet.Default.useFileMappedBuffer", "false");    // webApp.setInitParams(Collections.singletonMap("org.mortbay.jetty.servlet.Default.useFileMappedBuffer", "false"));
            context.setParentLoaderPriority(true);
            addAccessLog(args, server);
            server.setHandler(context);
            server.start();
            server.join();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            server.dumpStdErr();
        }
    }

    private static void addAccessLog(String[] args, Server server) {
        String requestLogPath = Functions.Kit.get("requestLog");
        if (StringUtils.isNotBlank(requestLogPath)) {
            NCSARequestLog requestLog = new AsyncNCSARequestLog(
                    requestLogPath + group + "-" + port + "-yyyy_mm_dd.request.log");
            requestLog.setAppend(true);
            requestLog.setExtended(false);
            requestLog.setPreferProxiedForAddress(true);
            requestLog.setLogTimeZone("GMT");
            requestLog.setLogLatency(true);
            requestLog.setRetainDays(90);
            requestLog.setLogServer(true);
            requestLog.setExtended(true);
            server.setRequestLog(requestLog);
        }
    }
}